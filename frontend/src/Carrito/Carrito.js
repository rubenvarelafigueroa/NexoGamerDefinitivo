import React, { useEffect, useState } from 'react'; // Importación de React y los hooks useEffect y useState
import { Link } from 'react-router-dom'; // Importación del componente Link de React Router
import CabeceraGlobal from "../CabeceraGlobal/CabeceraGlobal"; // Importación del componente CabeceraGlobal
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; // Importación del componente FontAwesomeIcon
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro'; // Importación de iconos con FontAwesome
import "./Carrito.css"; // Estilos CSS
import logo from "../nexoGamerFinal.png"; // Importación del logo
import basura from "./basura.png"; // Importación de la imagen de la papelera para eliminar juegos

const Carrito = () => {
    // Definición de estados utilizando el hook useState
    const [juegosCarrito, setJuegosCarrito] = useState([]);
    const [precioOficial, setPrecioOficial] = useState(0);
    const [descuento, setDescuento] = useState(0);
    const [total, setTotal] = useState(0);
    const [idUser, setIdUser] = useState(localStorage.getItem('iduser')); // Obtener el idUser de localStorage
    const [cantidadesSeleccionadas, setCantidadesSeleccionadas] = useState({});

    // Efecto de lado que se ejecuta cuando el componente se monta
    useEffect(() => {
        const obtenerJuegosCarrito = async () => {
            try {
                // Obtener los juegos del carrito del usuario desde la API
                const response = await fetch(`http://localhost:8000/carrito/${idUser}`);
                if (!response.ok) {
                    throw new Error('Error al obtener los juegos del carrito');
                }
                const data = await response.json();

                if (Array.isArray(data)) {
                    // Consolidar juegos duplicados y sumar cantidades
                    const juegosConsolidados = [];
                    data.forEach(juego => {
                        const index = juegosConsolidados.findIndex(item => item.juegoId === juego.juegoId);
                        if (index !== -1) {
                            // Si el juego ya está en la lista, sumar la cantidad
                            juegosConsolidados[index].cantidad += juego.cantidad;
                        } else {
                            // Si el juego no está en la lista, agregarlo
                            juegosConsolidados.push(juego);
                        }
                    });
                    setJuegosCarrito(juegosConsolidados);
                    calcularTotales(juegosConsolidados); // Calcular los totales
                } else {
                    console.error('Respuesta de la API no es un array:', data);
                }
            } catch (error) {
                console.error('Error al obtener los juegos del carrito:', error);
            }
        };
    
        obtenerJuegosCarrito(); // Llamar a la función para obtener los juegos del carrito
    }, []); // La dependencia es un arreglo vacío, por lo que el efecto se ejecuta solo una vez al montar el componente

    // Función para calcular los totales del carrito
    const calcularTotales = (juegos) => {
        let precioTotalJuegos = juegos.reduce((total, juego) => {
            return total + parseFloat(juego.precio * juego.cantidad);
        }, 0);
    
        let descuentoTotal = juegos.reduce((total, juego) => {
            return total + parseFloat(juego.precio * (juego.rebaja / 100) * juego.cantidad);
        }, 0);
    
        let precioOficialTotal = precioTotalJuegos - descuentoTotal;
    
        setPrecioOficial(precioTotalJuegos.toFixed(2));
        setDescuento(descuentoTotal.toFixed(2));
        setTotal(precioOficialTotal.toFixed(2));
    };

    // Función para manejar el cambio de cantidad de un juego en el carrito
    const handleChange = (juegoId, nuevaCantidad) => {
        // Verificar si la nueva cantidad es menor o igual a la cantidad de juegos en el carrito
        if (nuevaCantidad <= juegosCarrito.find(juego => juego.juegoId === juegoId).cantidad) {
            // Actualizar la cantidad del juego en juegosCarrito
            const nuevosJuegosCarrito = juegosCarrito.map(juego => {
                if (juego.juegoId === juegoId) {
                    juego.cantidad = nuevaCantidad;
                }
                return juego;
            });
            setJuegosCarrito(nuevosJuegosCarrito);
    
            // Actualizar el estado cantidadesSeleccionadas
            const nuevasCantidadesSeleccionadas = { ...cantidadesSeleccionadas, [juegoId]: nuevaCantidad };
            setCantidadesSeleccionadas(nuevasCantidadesSeleccionadas);
    
            calcularTotales(nuevosJuegosCarrito);
        } else {
            console.log('No se pueden seleccionar más de la cantidad de juegos en el carrito.');
        }
    };
    
    // Función para eliminar un juego del carrito
    const eliminarJuego = (juegoId) => {
        const nuevosJuegosCarrito = juegosCarrito.filter(juego => juego.juegoId !== juegoId);
        setJuegosCarrito(nuevosJuegosCarrito);
        calcularTotales(nuevosJuegosCarrito);
    };

    return (
        <>
            <CabeceraGlobal />
            <div className='carritoContainer'>
                <div className='cesta'>
                    {juegosCarrito.length === 0 ? (
                        <div className='cestaVacia'>
                            <FontAwesomeIcon icon={icon({ name: 'shopping-cart', family: 'classic', style: 'solid' })} size="2x" color="#FF5400" />
                            <h2>Tu cesta está vacía</h2>
                            <p>No has añadido ningún producto a tu cesta todavía. ¡Navega por la web y encuentra ofertas increíbles!</p>
                            <Link to="/inicio" className='descubreJuegosBtn'>Descubre juegos</Link>
                        </div>
                    ) : (
                    <div className='cestaLlena' style={{ textAlign: 'center' }}>
                        {Object.values(juegosCarrito.map(juego => (
                            <div key={juego.juegoId} className='juegoCarrito'>
                                <img src={juego.urlImagen} alt={juego.nombre} className="imagenPequeña" />
                                <div className='juegoInfo'>
                                    <p className='carInfoHead'>{juego.nombre}</p>
                                    <p className='carInfo'><strong>Consola:</strong> {juego.consola.split('/')[0]}</p> {/* Solo muestra la primera consola */}
                                    <p className='carInfo'><strong>Género:</strong> {juego.genero.split('/')[0]}</p>
                                    <p className='carInfo'><strong>Precio individual:</strong> {juego.precio}</p>
                                    <p className='carInfo'><strong>Descuento:</strong> {juego.rebaja}%</p> {/* Solo muestra la primera compañía */}
                                    <select value={cantidadesSeleccionadas[juego.juegoId] || juego.cantidad} onChange={(e) => handleChange(juego.juegoId, parseInt(e.target.value))} className='select'>
                                        {[...Array(juego.cantidad)].map((_, index) => (
                                        <option key={index + 1} value={index + 1}>{index + 1}</option>
                                        ))}
                                    </select>
                                    <img src={basura} alt='basura' className='basura' onClick={() => eliminarJuego(juego.juegoId)}></img>
                                </div>
                            </div>
                        )))}
                    </div>
                    )}
                </div>
                <div className='resumen'>
    <h2>Resumen</h2>
    <div className='resumenPrecios'>
        <div className='resumenFila'>
            <span>Precio oficial</span>
            <span>{precioOficial}€</span>
        </div>
        <div className='resumenFila'>
            <span>Descuento</span>
            <span>{descuento}€</span>
        </div>
        <div className='resumenFila total'>
            <span className='total'>Total</span>
            <span>{total}€</span>
        </div>
    </div>
    <button className='proceedToCheckoutBtn'>Proceder con el pago</button>
</div>
            </div>
            <div className="footer">
                <div className="leftFooter">
                    <Link to="/">Aviso legal</Link>
                    <Link to="/">Cookies</Link>
                </div>
                <div className="rightFooter">
                    <Link to="/"><img src={logo} alt="logo" className="miniLogo"></img></Link>
                    <Link to="/" className="sin-subrayado"><h2>NEXOGAMER</h2></Link>
                </div>
            </div>
        </>
    );
};

export default Carrito;
