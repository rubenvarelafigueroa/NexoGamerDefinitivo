import "./App.css";
import Cabecera from "./Cabecera/Cabecera";

//Sirve para cargar la página de inicio entera
const Inicio = () => {

    return <div className="homePage">
                <Cabecera></Cabecera>
            </div>

}

export default Inicio;