import React, { useState, useRef } from "react"; // Importa React y los hooks useState y useRef
import "./LoginRegister.css"; // Importa los estilos CSS para el componente
import logo from "../nexoGamerFinal.png"; // Importa la imagen del logo
import { Link } from "react-router-dom"; // Importa el componente Link de react-router-dom para la navegación entre páginas
import fondo from "./fondo/fondo2.mp4"; // Importa el video de fondo
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; // Importa FontAwesomeIcon para usar iconos
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro'; // Importa la función icon para definir los iconos a utilizar

const Login = () => { // Define el componente funcional Login
    const videoRef = useRef(null); // Crea una referencia al elemento de video
    const [muted, setMuted] = useState(false); // Define un estado para controlar si el video está silenciado
    const [idUser, setIdUser] = useState(localStorage.getItem('iduser')); // Define un estado para almacenar el ID del usuario
    const [sessionToken, setSessionToken] = useState(localStorage.getItem('token')); // Define un estado para almacenar el token de sesión del usuario
    const [formData, setFormData] = useState({ // Define un estado para almacenar los datos del formulario de inicio de sesión
        nombre: "",
        contraseña: ""
    });
    const [updateFormData, setUpdateFormData] = useState({ // Define un estado para almacenar los datos del formulario de actualización de cuenta
        nombre: "",
        apellidos: "",
        contraseña: "",
        telefono: "",
        email: ""
    });
    const [formularioEnviado, setFormularioEnviado] = useState(false); // Define un estado para controlar si el formulario se ha enviado
    const [errorAlEnviar, setErrorAlEnviar] = useState(false); // Define un estado para controlar si ocurrió un error al enviar el formulario
    const [mensajeError, setMensajeError] = useState(""); // Define un estado para almacenar mensajes de error
    const [mensajeExito, setMensajeExito] = useState(""); // Define un estado para almacenar mensajes de éxito
    const [sesionCerrada, setSesionCerrada] = useState(false); // Define un estado para controlar si la sesión está cerrada
    const [errorAlCerrarSesion, setErrorAlCerrarSesion] = useState(false); // Define un estado para controlar si ocurrió un error al cerrar sesión

    // Función para alternar el silencio del video
    const toggleMute = () => {
        if (videoRef.current) {
            videoRef.current.muted = !videoRef.current.muted;
            setMuted(videoRef.current.muted);
        }
    };

    // Función para manejar cambios en los campos del formulario de inicio de sesión
    const handleInputChange = (e) => {
        const { name, value } = e.target;

        // Validación de campos específicos
        if (name === "apellidos" && !/^[a-zA-Z\s]*$/.test(value)) return;
        if (name === "telefono" && !/^\d{0,9}$/.test(value)) return;

        setFormData({
            ...formData,
            [name]: value
        });
    };

    // Función para manejar cambios en los campos del formulario de actualización de cuenta
    const handleUpdateInputChange = (e) => {
        const { name, value } = e.target;

        // Validación de campos específicos
        if (name === "apellidos" && !/^[a-zA-Z\s]*$/.test(value)) return;
        if (name === "telefono" && !/^\d{0,9}$/.test(value)) return;

        setUpdateFormData({
            ...updateFormData,
            [name]: value
        });
    };

    // Función para enviar el formulario de inicio de sesión al servidor
    const handleSubmit = (e) => {
        e.preventDefault(); // Evita que el formulario recargue la página

        // Configuración de la solicitud
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formData)
        };

        setSesionCerrada(false); // Restablece el estado de sesión cerrada

        // Envía la solicitud al servidor
        fetch('http://localhost:8000/login', requestOptions)
            .then(response => response.json().then(data => {
                if (response.ok) {
                    // Si la respuesta es exitosa, muestra un mensaje de éxito y guarda el ID de usuario y el token de sesión en el almacenamiento local
                    setMensajeExito("¡Inicio de sesión exitoso!");
                    setFormularioEnviado(true);
                    setTimeout(() => {
                        setFormularioEnviado(false);
                        const { iduser, token } = data;
                        localStorage.setItem('iduser', iduser);
                        localStorage.setItem('token', token);
                        setIdUser(iduser);
                        setSessionToken(token);
                    }, 2000);
                    setFormData({
                        nombre: "",
                        contraseña: ""
                    });
                } else {
                    throw new Error(data.error || 'Credenciales incorrectas');
                }
            }))
            .catch(error => {
                // Si ocurre un error, muestra un mensaje de error
                console.error('Error al iniciar sesión:', error);
                setMensajeError(error.message);
                setErrorAlEnviar(true);
                setTimeout(() => setErrorAlEnviar(false), 5000);
            });
    };

    // Función para enviar el formulario de actualización de cuenta al servidor
    const handleUpdateSubmit = (e) => {
        e.preventDefault(); // Evita que el formulario recargue la página
        if (!idUser) {
            console.error("El idUser no está definido");
            return;
        }
        const updateData = { ...updateFormData, id_user: idUser };
        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(updateData)
        };
        fetch(`http://localhost:8000/update_user/${idUser}`, requestOptions)
            .then(response => response.json().then(data => {
                if (response.ok) {
                    setMensajeExito("¡Actualización exitosa!");
                    setFormularioEnviado(true);
                    setTimeout(() => {
                        setFormularioEnviado(false);
                    }, 2000);
                    setUpdateFormData({
                        nombre: "",
                        apellidos: "",
                        contraseña: "",
                        telefono: "",
                        email: ""
                    });
                } else {
                    throw new Error(data.error || 'Error al actualizar');
                }
            }))
            .catch(error => {
                console.error('Error al actualizar:', error);
                setMensajeError(error.message);
                setErrorAlEnviar(true);
                setTimeout(() => setErrorAlEnviar(false), 5000);
            });
    };

    //Función para cerrar la sesión del usuario
    const handleLogout = () => {
        if (!idUser || !sessionToken) {
            console.error("No se puede cerrar sesión, idUser o token de sesión no definidos");
            setErrorAlCerrarSesion(true);
            setMensajeError("No se puede cerrar sesión, idUser o token de sesión no definidos");
            setTimeout(() => {
                setErrorAlCerrarSesion(false);
                setMensajeError("");
            }, 5000);
            return;
        }
        const requestOptions = {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionToken}`
            },
            body: JSON.stringify({ idUser })
        };
        fetch(`http://localhost:8000/logout/${idUser}`, requestOptions)
            .then(response => {
                if (response.ok) {
                    localStorage.removeItem('iduser');
                    localStorage.removeItem('token');
                    setIdUser(null);
                    setSessionToken(null);
                    setMensajeExito("¡Sesión cerrada exitosamente!");
                    setSesionCerrada(true); // Establecer el estado de la sesión cerrada como true
                    setErrorAlCerrarSesion(false);
                    setTimeout(() => setSesionCerrada(false), 5000);
                } else {
                    throw new Error('Error al cerrar sesión');
                }
            })
            .catch(error => {
                console.error('Error al cerrar sesión:', error);
                setMensajeError(error.message);
                setErrorAlCerrarSesion(true);
                setTimeout(() => {
                    setErrorAlCerrarSesion(false);
                    setMensajeError("");
                }, 5000);
            });
    };

    //Función para eliminar la cuenta del usuario
    const handleDeleteAccount = (e) => {
        if (!idUser) {
            console.error("El idUser no está definido");
            return;
        }
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' }
        };
        fetch(`http://localhost:8000/del_user/${idUser}`, requestOptions)
            .then(response => {
                if (response.ok) {
                    localStorage.removeItem('iduser');
                    localStorage.removeItem('token');
                    setIdUser(null);
                    setSessionToken(null);
                    setMensajeExito("¡Cuenta eliminada exitosamente!");
                    setSesionCerrada(true);
                    setErrorAlCerrarSesion(false); // Aquí establece errorAlCerrarSesion como false
                    setTimeout(() => setSesionCerrada(false), 5000);
                } else {
                    throw new Error('Error al eliminar cuenta');
                }
            })
            .catch(error => {
                console.error('Error al eliminar cuenta:', error);
                setMensajeError(error.message);
                setErrorAlCerrarSesion(true);
                setTimeout(() => {
                    setErrorAlCerrarSesion(false);
                    setMensajeError("");
                }, 5000);
            });
    };

    return (
        <>
            <script src="https://kit.fontawesome.com/19066b4921.js" crossOrigin="anonymous"></script>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
            <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900&display=swap" rel="stylesheet"></link>
            <div className="cabeceraRegister">
                <Link to={"/"}><img src={logo} alt="logo"></img></Link>
                <Link to={"/"} className="sin-subrayado"><h1>NEXOGAMER</h1></Link>
            </div>
            <div className="videoDerecha">
                <div className="formularioLogin" style={{ position: "relative" }}>
                    {idUser ? (
                        <div className="formularioUpdate">
                            {!formularioEnviado && !errorAlEnviar && !sesionCerrada && (
                                <>
                                    {/* <div className="logoutButtonContainer">
                                        <button onClick={handleLogout} className="logoutButton">Cerrar sesión</button>
                                    </div> */}
                                    <h1>Actualizar tu cuenta</h1>
                                    <form onSubmit={handleUpdateSubmit} className={`updateForm ${formularioEnviado ? 'fadeOut' : 'fadeIn'}`}>
                                        <label htmlFor="updateNombre">Nombre</label>
                                        <input type="text" id="updateNombre" name="nombre" value={updateFormData.nombre} onChange={handleUpdateInputChange} className="campo" placeholder="Rubén"/>
    
                                        <label htmlFor="updateApellidos">Apellidos</label>
                                        <input type="text" id="updateApellidos" name="apellidos" value={updateFormData.apellidos} onChange={handleUpdateInputChange} className="campo" placeholder="Varela"/>
    
                                        <label htmlFor="updateContraseña">Contraseña</label>
                                        <input type="password" id="updateContraseña" name="contraseña" value={updateFormData.contraseña} onChange={handleUpdateInputChange} className="campo" placeholder="S€gUr4"/>
    
                                        <label htmlFor="updateTelefono">Teléfono</label>
                                        <input type="text" id="updateTelefono" name="telefono" value={updateFormData.telefono} onChange={handleUpdateInputChange} className="campo" placeholder="634597141"/>
    
                                        <label htmlFor="updateEmail">Email</label>
                                        <input type="email" id="updateEmail" name="email" value={updateFormData.email} onChange={handleUpdateInputChange} className="campo" placeholder="ruben@gmail.com"/>
    
                                        <input type="submit" value="Enviar" className="submitRegister"/>
                                        <h2 className="no-space">Más opciones</h2>
                                        <input type="submit" value="Cerrar sesión" className="submitLarge" onClick={handleLogout}/>
                                        <input type="submit" value="Borrar cuenta" className="submitLarge" onClick={handleDeleteAccount}/>
                                    </form>
                                </>
                            )}
                            {formularioEnviado && !sesionCerrada && (
                                <div className={`exitoFormulario ${formularioEnviado ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#00FF00" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#00FF00" strokeWidth="2" />
                                        <path d="M7.5 12.5l3.5 3.5 6-6" strokeWidth="2.5" ></path>
                                    </svg>
                                    <h2>{mensajeExito}</h2>
                                </div>
                            )}
                            {errorAlEnviar && !sesionCerrada && (
                                <div className={`errorFormulario ${errorAlEnviar ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#FF0000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#FF0000" strokeWidth="2" />
                                        <line x1="15" y1="9" x2="9" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash1 1.5s ease-in-out forwards` }} />
                                        <line x1="9" y1="9" x2="15" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash2 1.5s ease-in-out forwards` }} />
                                    </svg>
                                    <h2>¡Error al actualizar cuenta!</h2>
                                    <p>{mensajeError}</p>
                                </div>
                            )}
                        </div>
                    ) : (
                        <div className="formularioLogin">
                            {!formularioEnviado && !errorAlEnviar && (
                                <>
                                    <h1>Iniciar sesión</h1>
                                    <form onSubmit={handleSubmit} className="loginForm">
                                        <label htmlFor="nombre">Nombre de usuario</label>
                                        <input type="text" id="nombre" name="nombre" value={formData.nombre} onChange={handleInputChange} placeholder="Rubén" className="campo"></input>
                                        <label htmlFor="password">Contraseña</label>
                                        <input type="password" id="password" name="contraseña" value={formData.contraseña} onChange={handleInputChange} placeholder="S€gUr4" className="campo"></input>
                                        <input type="submit" value="Enviar" className="submitLogin"></input>
                                    </form>
                                    <p className="message">¿No tienes una cuenta? <Link to={"/register"} className="cambiarAzul">Regístrate</Link></p>
                                </>
                            )}
                            {formularioEnviado && !sesionCerrada && (
                                <div className={`exitoFormulario ${formularioEnviado ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#00FF00" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#00FF00" strokeWidth="2" />
                                        <path d="M7.5 12.5l3.5 3.5 6-6" strokeWidth="2.5" ></path>
                                    </svg>
                                    <h2>{mensajeExito}</h2>
                                </div>
                            )}
                            {errorAlEnviar && !sesionCerrada && (
                                <div className={`errorFormulario ${errorAlEnviar ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#FF0000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#FF0000" strokeWidth="2" />
                                        <line x1="15" y1="9" x2="9" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash1 1.5s ease-in-out forwards` }} />
                                        <line x1="9" y1="9" x2="15" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash2 1.5s ease-in-out forwards` }} />
                                    </svg>
                                    <h2>¡Error al iniciar sesión!</h2>
                                    <p>{mensajeError}</p>
                                </div>
                            )}
                            {sesionCerrada && (
                                <div className={`exitoFormulario ${sesionCerrada ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#00FF00" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#00FF00" strokeWidth="2" />
                                        <path d="M7.5 12.5l3.5 3.5 6-6" strokeWidth="2.5" ></path>
                                    </svg>
                                    <h2>{mensajeExito}</h2>
                                </div>
                            )}
                            {errorAlCerrarSesion && (
                                <div className={`errorFormulario ${errorAlCerrarSesion ? 'fadeIn' : 'fadeOut'}`}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#FF0000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                        <circle cx="12" cy="12" r="11" fill="none" stroke="#FF0000" strokeWidth="2" />
                                        <line x1="15" y1="9" x2="9" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash1 1.5s ease-in-out forwards` }} />
                                        <line x1="9" y1="9" x2="15" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash2 1.5s ease-in-out forwards` }} />
                                    </svg>
                                    <h2>¡Error al cerrar sesión!</h2>
                                    <p>{mensajeError}</p>
                                </div>
                            )}
                        </div>
                    )}
                </div>
                <video ref={videoRef} src={fondo} autoPlay loop controls={false}></video>
                {muted ? <FontAwesomeIcon className="muted" icon={icon({ name: 'volume-xmark', family: 'classic', style: 'solid' })} onClick={toggleMute} /> : <FontAwesomeIcon className="no-muted" icon={icon({ name: 'volume-high', family: 'classic', style: 'solid' })} onClick={toggleMute} />}
            </div>
            <div className="footer">
                <div className="leftFooter">
                    <Link to="/">Aviso legal</Link>
                    <Link to="/">Cookies</Link>
                </div>
                <div className="rightFooter">
                    <Link to="/"><img src={logo} alt="logo" className="miniLogo"></img></Link>
                    <Link to="/" className="sin-subrayado"><h2>NEXOGAMER</h2></Link>
                </div>
            </div>
        </>
    );
}

export default Login;
