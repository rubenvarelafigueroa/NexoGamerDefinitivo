import React, { useState, useRef } from "react"; // Importa React y los hooks useState y useRef
import "./LoginRegister.css"; // Importa los estilos CSS
import logo from "../nexoGamerFinal.png"; // Importa el logo
import { Link } from "react-router-dom"; // Importa el componente Link de React Router
import fondo from "./fondo/fondo.mp4"; // Importa el video de fondo
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; // Importa FontAwesomeIcon para los íconos
import { icon } from '@fortawesome/fontawesome-svg-core/import.macro'; // Importa iconos con FontAwesome

const Register = () => {
    const videoRef = useRef(null); // Referencia al video
    const [muted, setMuted] = useState(false); // Estado
    const [formData, setFormData] = useState({ // Estado para los datos del formulario
        nombre: "",
        apellidos: "",
        contraseña: "",
        telefono: "",
        email: ""
    });
    const [mostrarExito, setMostrarExito] = useState(false); // Estado para mostrar mensaje de éxito
    const [errorAlEnviar, setErrorAlEnviar] = useState(false); // Estado para mostrar mensaje de error
    const [mensajeError, setMensajeError] = useState("");  // Estado para mensaje de error específico

    // Función para alternar el estado de silencio del video
    const toggleMute = () => {
        if (videoRef.current) {
            videoRef.current.muted = !videoRef.current.muted;
            setMuted(videoRef.current.muted);
        }
    };

    // Función para manejar cambios en los inputs del formulario
    const handleInputChange = (e) => {
        const { name, value } = e.target;

        // Validación para evitar números en el campo apellidos
        if (name === "apellidos" && /\d/.test(value)) {
            return;
        }

        // Validación para limitar el campo teléfono a 9 dígitos
        if (name === "telefono" && (/\D/.test(value) || value.length > 9)) {
            return;
        }

        // Actualizar el estado de formData con el nuevo valor del input
        setFormData({
            ...formData,
            [name]: value
        });
    };

    // Función para manejar el envío del formulario
    const handleSubmit = (e) => {
        e.preventDefault();
        // Construir la solicitud POST con los datos del formulario
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formData)
        };
        // Realizar la solicitud al servidor
        fetch('http://localhost:8000/register', requestOptions)
            .then(response => response.json().then(data => {
                if (response.ok) {
                    // Mostrar mensaje de éxito si la respuesta es exitosa
                    setMostrarExito(true);
                    setTimeout(() => setMostrarExito(false), 5000); // Ocultar mensaje de éxito después de 5 segundos
                    // Limpiar los campos del formulario
                    setFormData({
                        nombre: "",
                        apellidos: "",
                        contraseña: "",
                        telefono: "",
                        email: ""
                    });
                } else {
                    // Manejar errores de la respuesta
                    throw new Error(data.error || 'No se pudo registrar el usuario');
                }
            }))
            .catch(error => {
                // Manejar errores de la solicitud
                console.error('Error al registrar usuario:', error);
                // Actualizar el estado con el mensaje de error específico
                setMensajeError(error.message);
                setErrorAlEnviar(true);
                setTimeout(() => setErrorAlEnviar(false), 5000); // Ocultar mensaje de error después de 5 segundos
            });
    };

    return (
        <>
            <script src="https://kit.fontawesome.com/19066b4921.js" crossOrigin="anonymous"></script>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="true" />
            <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900&display=swap" rel="stylesheet"></link>
            <div className="cabeceraRegister">
                <Link to={"/"}><img src={logo} alt="logo"></img></Link>
                <Link to={"/"} className="sin-subrayado"><h1>NEXOGAMER</h1></Link>
            </div>
            <div className="videoDerecha">
                <div className="formularioRegistro" style={{ position: "relative" }}>
                    {!mostrarExito && !errorAlEnviar && (
                        <>
                            <h1>Regístrate</h1>
                            <form onSubmit={handleSubmit} className="registroForm">
                                <label htmlFor="nombre">Nombre de usuario</label>
                                <input type="text" id="nombre" name="nombre" value={formData.nombre} onChange={handleInputChange} placeholder="Rubén" className="campo"></input>
                                <label htmlFor="apellidos">Apellidos</label>
                                <input type="text" id="apellidos" name="apellidos" value={formData.apellidos} onChange={handleInputChange} placeholder="Varela" className="campo"></input>
                                <label htmlFor="password">Contraseña</label>
                                <input type="password" id="password" name="contraseña" value={formData.contraseña} onChange={handleInputChange} placeholder="S€gUr4" className="campo"></input>
                                <label htmlFor="telefono">Teléfono</label>
                                <input type="text" id="telefono" name="telefono" value={formData.telefono} onChange={handleInputChange} placeholder="634597141" className="campo"></input>
                                <label htmlFor="gmail">Correo electrónico</label>
                                <input type="email" id="gmail" name="email" value={formData.email} onChange={handleInputChange} placeholder="ruben@gmail.com" className="campo"></input>
                                <input type="submit" value="Enviar" className="submitRegister"></input>
                            </form>
                            <p className="message">¿Ya tienes una cuenta? <Link to={"/login"} className="cambiarAzul">Inicia sesión</Link></p>
                        </>
                    )}
                    {mostrarExito && (
                        <div className={`exitoFormulario ${mostrarExito ? 'fadeIn' : 'fadeOut'}`}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#00FF00" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                <circle cx="12" cy="12" r="11" fill="none" stroke="#00FF00" strokeWidth="2" />
                                <path d="M7.5 12.5l3.5 3.5 6-6" strokeWidth="2.5"></path>
                            </svg>
                            <h2>¡Cuenta creada con éxito!</h2>
                        </div>
                    )}
                    {errorAlEnviar && (
                        <div className={`errorFormulario ${errorAlEnviar ? 'fadeIn' : 'fadeOut'}`}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#FF0000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                                <circle cx="12" cy="12" r="11" fill="none" stroke="#FF0000" strokeWidth="2" style={{ animation: `drawCheck 1.5s ease-in-out forwards` }}/>
                                <line x1="15" y1="9" x2="9" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash1 1.5s ease-in-out forwards` }} />
                                <line x1="9" y1="9" x2="15" y2="15" stroke="red" strokeWidth="2" style={{ animation: `drawSlash2 1.5s ease-in-out forwards` }} />
                            </svg>
                            <h2>¡Error al crear la cuenta!</h2>
                            <p>{mensajeError}</p>
                        </div>
                    )}
                </div>
                <video ref={videoRef} src={fondo} autoPlay loop controls={false}></video>
                {muted ? <FontAwesomeIcon className="muted" icon={icon({ name: 'volume-xmark', family: 'classic', style: 'solid' })} onClick={toggleMute}/> : <FontAwesomeIcon className="no-muted" icon={icon({ name: 'volume-high', family: 'classic', style: 'solid' })} onClick={toggleMute}/>}
            </div>
            <div className="footer">
                <div className="leftFooter">
                    <Link to="/">Aviso legal</Link>
                    <Link to="/">Cookies</Link>
                </div>
                <div className="rightFooter">
                    <Link to="/"><img src={logo} alt="logo" className="miniLogo"></img></Link>
                    <Link to="/" className="sin-subrayado"><h2>NEXOGAMER</h2></Link>
                </div>
            </div>
        </>
    );
}

export default Register;